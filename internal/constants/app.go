package constants

import "template/internal/configs"

var (
	// AppInfo App信息
	AppInfo *configs.AppInfo
)

const (
	// DeviceType 设备类型
	DeviceType = "b2"
	// Author HTTP API 请求者
	Author = "template-api"
)
