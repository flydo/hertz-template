package helpers

import (
	"github.com/pelletier/go-toml/v2"
	"os"
)

// ReadToml toml file read
func ReadToml(name string, dest any) error {
	file, err := os.Open(name)
	if err != nil {
		return err
	}
	defer file.Close()
	return toml.NewDecoder(file).Decode(&dest)
}
