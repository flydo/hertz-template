package boot

import (
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"os"
	"template/internal/constants"
	"template/internal/utils/helpers"
	"template/third_party/lumberjack"
)

// configure 配置信息初始化
func configure() error {
	return helpers.ReadToml(constants.AppInfo.ConfigName, &constants.Cfg)
}

// logger 日志初始化
func logger() error {
	var writers []zapcore.WriteSyncer

	mylog := constants.Cfg.Log
	// 打印到屏幕
	if mylog.Mode == 0 || mylog.Mode == 1 {
		writers = append(writers, zapcore.AddSync(os.Stdout))
	}

	zl := mylog.Zap
	ec := zap.NewProductionEncoderConfig()
	ec.CallerKey = zl.CallerKey
	ec.LevelKey = zl.LevelKey
	ec.NameKey = zl.NameKey
	ec.MessageKey = zl.MessageKey
	ec.StacktraceKey = zl.StacktraceKey
	ec.TimeKey = zl.TimeKey

	// 时间格式
	if zl.EncodeTime != "" {
		ec.EncodeTime = zapcore.TimeEncoderOfLayout(zl.EncodeTime)
	}

	// 保存到文件(hook)
	if mylog.Mode == 0 || mylog.Mode == 2 {
		hook := mylog.Hook
		lg := lumberjack.Logger{
			Filename:   hook.FileName,
			MaxSize:    hook.MaxSize,
			MaxAge:     hook.MaxAge,
			MaxBackups: hook.MaxBackups,
			Compress:   hook.Compress,
		}
		writers = append(writers, zapcore.AddSync(&lg))
	}

	// 设置日志级别
	al := zap.NewAtomicLevel()
	al.SetLevel(zapcore.Level(mylog.Level))

	var zapOptions []zap.Option
	// 开启开发模式，堆栈跟踪。CallerKey 值必须不为空字符串。
	if mylog.Debug {
		zapOptions = append(zapOptions, zap.AddCaller())
	}
	// 设置初始化字段（扩展自带）
	if mylog.Name != "" {
		zapOptions = append(zapOptions, zap.Fields(zap.String("name", mylog.Name)))
	}
	//zapOptions = append(zapOptions, zap.Development()) // 开启文档及行号

	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(ec),              // 编码器配置
		zapcore.NewMultiWriteSyncer(writers...), // 打印到控制面板和文档
		al,                                      // 日志级别
	)

	logger := zap.New(core, zapOptions...) // 构造日志
	defer func(logger *zap.Logger) {
		_ = logger.Sync()
	}(logger)
	zap.ReplaceGlobals(logger)
	return nil
}
