package boot

import (
	"github.com/cloudwego/hertz/pkg/app/server"
	"github.com/cloudwego/hertz/pkg/app/server/registry"
	"github.com/cloudwego/hertz/pkg/common/config"
	"github.com/cloudwego/hertz/pkg/common/utils"
	"github.com/hertz-contrib/registry/etcd"
	"strings"
	"template/internal/constants"
	"template/internal/routers"
)

// runHTTPServer 启动 HTTP 服务器
func runHTTPServer() error {
	var opts []config.Option
	etcdCfg := constants.Cfg.Etcd
	httpCfg := constants.Cfg.Server.HTTP

	opts = append(opts, server.WithHostPorts(httpCfg.ServerURI))
	// etcd server uri 和 http server name 都存在
	if etcdCfg.ServerURI != "" && httpCfg.ServiceName != "" {
		var etcdOpts []etcd.Option
		// etcd 用户名和密码存在
		if etcdCfg.Username != "" && etcdCfg.Password != "" {
			etcdOpts = append(etcdOpts, etcd.WithAuthOpt(etcdCfg.Username, etcdCfg.Password))
		}
		reg, err := etcd.NewEtcdRegistry(strings.Split(etcdCfg.ServerURI, ","))
		if err != nil {
			return err
		}
		opts = append(opts, server.WithRegistry(reg, &registry.Info{
			ServiceName: strings.ToLower(httpCfg.ServiceName),
			Addr:        utils.NewNetAddr("tcp", httpCfg.ServerURI),
			Weight:      10,
			Tags:        nil,
		}))
	}

	h := server.Default(opts...)

	if err := routers.Register(h); err != nil {
		return err
	}

	h.Spin()
	return nil
}
