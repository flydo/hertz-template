package boot

import (
	"fmt"
	"template/internal/constants"
)

var servMsg = `
------------------------------------------------------------------------
------------------------------------------------------------------------
--------------------     %s     ---------------------
--------              HTTP  Server: %s       
--------              Start Time  : %s           
--------              Version     : %s
------------------------------------------------------------------------
------------------------------------------------------------------------

`

// Start 启动服务
func Start() error {
	if err := Config(); err != nil {
		return err
	}

	// Show Message
	servUri := constants.Cfg.Server.HTTP.ServerURI
	if servUri == "" {
		servUri = "<random port>"
	}

	fmt.Printf(servMsg,
		constants.Cfg.App.Name,
		servUri,
		constants.AppInfo.StartTime,
		constants.AppInfo.Version,
	)

	return runHTTPServer()
}

// Config 初始化配置
func Config() error {
	if err := configure(); err != nil {
		return err
	}

	if err := logger(); err != nil {
		return err
	}

	return nil
}
