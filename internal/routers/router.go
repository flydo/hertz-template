package routers

import "github.com/cloudwego/hertz/pkg/app/server"

// Register 路由注册
func Register(h *server.Hertz) error {
	return RouterV1(h)
}
