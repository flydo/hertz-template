package routers

import (
	"context"
	"github.com/cloudwego/hertz/pkg/app"
	"github.com/cloudwego/hertz/pkg/app/server"
	"net/http"
	"template/internal/configs"
	"template/internal/constants"
)

// RouterV1 v1 版本接口
func RouterV1(h *server.Hertz) error {
	g := h.Group("/v1")
	g.GET("/ping", func(c context.Context, ctx *app.RequestContext) {
		ctx.JSON(http.StatusOK, map[string]any{"message": "pong"})
	})
	g.GET("/app", func(c context.Context, ctx *app.RequestContext) {
		ctx.JSON(http.StatusOK, configs.Response{
			Code:   0,
			ErrMsg: "",
			Data:   constants.AppInfo,
		})
	})
	return nil
}
