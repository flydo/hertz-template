package configs

// Response 请求返回值格式
type Response struct {
	Code   int    `json:"code"`
	ErrMsg string `json:"errMsg"`
	Data   any    `json:"data"`
}
