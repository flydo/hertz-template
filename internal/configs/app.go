package configs

// AppInfo App信息
type AppInfo struct {
	Version    string `json:"version"`
	StartTime  string `json:"start_time"`
	ConfigName string `json:"config_name"`
}
