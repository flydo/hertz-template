package configs

// Config file config.toml information
type Config struct {
	App      App      `toml:"app"`
	Server   Server   `toml:"server"`
	Token    Token    `toml:"token"`
	Etcd     Etcd     `toml:"etcd"`
	Log      Log      `toml:"log"`
	Services Services `toml:"services"`
}

// App 信息
type App struct {
	Name string `toml:"name"`
}

// Server 参数
type Server struct {
	HTTP ServInfo `toml:"http"`
}

// ServInfo 服务器信息
type ServInfo struct {
	ServerURI   string `toml:"server_uri"`
	ServiceName string `toml:"service_name"`
}

// Token 参数
type Token struct {
	Timeout     int  `toml:"timeout"`      // 小时
	AutoRefresh bool `toml:"auto_refresh"` // 自动刷新
	Length      int  `toml:"length"`       // token 长度
	SSR         bool `toml:"ssr"`          // 单会话登录
}

// Etcd 参数
type Etcd struct {
	ServerURI string `toml:"server_uri"`
	Username  string `toml:"username"`
	Password  string `toml:"password"`
}

// Log 参数
type Log struct {
	Debug bool   `toml:"debug"` // 开发模式
	Level int    `toml:"level"` // 日志等级
	Name  string `toml:"name"`  // 初始字段名
	Mode  int    `toml:"mode"`  // 日志存储模式

	Zap struct {
		CallerKey     string `toml:"caller_key"`
		LevelKey      string `toml:"level_key"`
		NameKey       string `toml:"name_key"`
		MessageKey    string `toml:"message_key"`
		StacktraceKey string `toml:"stacktrace_key"`
		TimeKey       string `toml:"time_key"`
		EncodeTime    string `toml:"encode_time"`
	} `toml:"zap"`

	Hook struct {
		FileName   string `toml:"file_name"`
		MaxSize    int    `toml:"max_size"`
		MaxBackups int    `toml:"max_backups"`
		MaxAge     int    `toml:"max_age"`
		Compress   bool   `toml:"compress"`
	} `toml:"hook"`
}

// Services 服务
type Services struct {
}
