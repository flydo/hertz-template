# HTTP API Template

HTTP API 模板

## 特征

- 使用 etcd 作为服务发现
- 使用 hertz 作为 web 框架

## 接口

| 接口名称 | 请求方法 | 接口网址 | 描述 |
| :------- | :------- | :------- | :--- |

## 依赖库

- https://github.com/cloudwego/hertz (web 框架)
- https://github.com/hertz-contrib/registry (服务发现)
  ```bash
  go get -u github.com/hertz-contrib/registry/etcd
  ```
- https://github.com/bytedance/sonic (JSON 解析)
- https://github.com/pelletier/go-toml (配置文件)
  ```bash
  go get -u github.com/pelletier/go-toml/v2
  ```
- https://github.com/uber-go/zap (日志)
  ```bash
  go get -u go.uber.org/zap
  ```

---

**目录规范参考 [Standard Go Project Layout](https://github.com/golang-standards/project-layout) 项目**

## 仓库镜像

- https://git.jetsung.com/flydo/hertz-template
- https://framagit.org/flydo/hertz-template
- https://codeup.aliyun.com/jetsung/flydo/hertz-template
