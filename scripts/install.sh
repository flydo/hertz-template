#!/usr/bin/env bash

####
## 全新独立安装脚本
##
## sudo ./scripts/install.sh
####

# service filename
# old: find ./*.service | awk '{print $1}' | head -n 1 | cut -d '/' -f 2
SERV_NAME="$(find . -maxdepth 1 -type f -name "*.service" -printf "%f\n" | head -n 1)"

# exec filename
# ls -l | awk '/^-rwx/{print $NF}'
# find . -maxdepth 1 -type f -perm /111 -print -quit | awk -F/ '{print $NF}'
EXEC_NAME=$(find . -maxdepth 1 -type f -perm /111 -printf "%f")

echo "UnitName: ${SERV_NAME}"

# 卸载
if [[ "${1}" = "-u" ]]; then
  systemctl disable "${SERV_NAME}"
  systemctl stop "${SERV_NAME}"
  rm "/etc/systemd/system/${SERV_NAME}"

else # 安装（默认）
  # cp *.service to systemd
  pushd ./ > /dev/null
    cp "${SERV_NAME}" /etc/systemd/system/
    sed -i "s@^ExecStart.*@ExecStart=$(pwd)/${EXEC_NAME}@" "/etc/systemd/system/${SERV_NAME}"
    sed -i "s@^WorkingDirectory.*@WorkingDirectory=$(pwd)@" "/etc/systemd/system/${SERV_NAME}"
  popd > /dev/null || exit

  # enable service
  systemctl daemon-reload
  systemctl enable "${SERV_NAME}"
  systemctl start "${SERV_NAME}"
fi