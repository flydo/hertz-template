package main

import (
	_ "embed"
	"flag"
	"log"
	"template/internal/boot"
	"template/internal/configs"
	"template/internal/constants"
	"time"
)

//go:embed version.txt
var version string

func init() {
	// 运行时加参数 -config=config.dev.toml 配置文件
	config := flag.String("config", "config.toml", "config file name")
	flag.Parse()
	constants.AppInfo = &configs.AppInfo{
		Version:    version,
		StartTime:  time.Now().Format("2006-01-02 15:04:05"),
		ConfigName: *config,
	}
}

func main() {
	if err := boot.Start(); err != nil {
		log.Fatalf("Server startup failed.\n%+v\n", err)
	}
}
